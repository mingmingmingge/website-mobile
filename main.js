import App from './App'
import uView from '@/uni_modules/uview-ui';
import store from '@/common/store.js'
import PreProcessing from '@/common/PreProcessing.js'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'

PreProcessing.initInterceptor();
Vue.prototype.cdn = store.cdn_url;
Vue.prototype.server_url = store.server_url;
Vue.prototype.store = store;
Vue.use(uView);

export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif