import store from "@/common/store.js"


var PreProcessing = {
	//拦截器
	initInterceptor() {
		uni.addInterceptor('request', {
			invoke(args) {
				// request 触发前拼接 url 
				args.url = store.server_url + args.url
				args.header.token = store.user.token ;
			},
			success(args) {
				
			},
			// fail(err) {
			// 	console.log('interceptor-fail', err)
			// },
			// complete(res) {
			// 	console.log('interceptor-complete', res)
			// }
		})
	}
}

export default PreProcessing
